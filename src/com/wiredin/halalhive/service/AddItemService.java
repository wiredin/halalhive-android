package com.wiredin.halalhive.service;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class AddItemService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	private int serverResponseCode = 0;
	Context context;
	String itemName;
	String itemBarcode;
	String itemInfo;
	String itemOrigin;
	String itemPic1;
	String itemPic2;

	public AddItemService(String itemName, String itemBarcode, String itemInfo,
			String itemOrigin, String itemPic1, Context context) {

		this.context = context;
		this.itemName = itemName;
		this.itemBarcode = itemBarcode;
		this.itemInfo = itemInfo;
		this.itemOrigin = itemOrigin;
		this.itemPic1 = itemPic1;

	}

	public AddItemService(String itemName, String itemBarcode, String itemInfo,
			String itemOrigin, String itemPic1, String itemPic2, Context context) {
		this.context = context;
		this.itemName = itemName;
		this.itemBarcode = itemBarcode;
		this.itemInfo = itemInfo;
		this.itemOrigin = itemOrigin;
		this.itemPic1 = itemPic1;
		this.itemPic2 = itemPic2;
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		// do initialization of required objects objects here
		Log.d("Fared", "Running Add Item");
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("Halal Hive"); // title
		progressDialog.setMessage("Adding item.."); // message
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				"http://wiredin.my/halalhive/services/additem");
		String text = null;
		Log.d("fared", "item name" +itemName);
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("barcode", this.itemBarcode));
			nameValuePairs
					.add(new BasicNameValuePair("itembar", this.itemName));
			nameValuePairs
					.add(new BasicNameValuePair("info", this.itemInfo));
			nameValuePairs
			.add(new BasicNameValuePair("category", this.itemOrigin));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}
		

		uploadFile(this.itemPic1);

		return text;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result != null) {

			Log.d("Hadian", "Jadi dgn result: " + result);
			progressDialog.dismiss();
			((Activity) context).finish();
		}
	}
	
	public int uploadFile(String sourceFileUri) {

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		
		String upLoadServerUri = "http://wiredin.my/halalhive/services/addpic1/"
				+ this.itemBarcode;

		if (!sourceFile.isFile()) {

			progressDialog = ProgressDialog.show(this.context, "",
					"Uploading file...", true);
			progressDialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + this.itemPic1);

			return 0;

		} else {
			try {
				Log.d("fared",  "uploadFile ="+ this.itemPic1);

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(upLoadServerUri);

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"photo\";filename=\"pic1.png\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {
					Log.d("fared", "Upload complete");

					progressDialog.dismiss();

					// runOnUiThread(new Runnable() {
					// public void run() {
					// String msg =
					// "File Upload Completed.\n\n See uploaded file here : \n\n"
					// +" F:/wamp/wamp/www/uploads";
					// messageText.setText(msg);
					// Toast.makeText(MainActivity.this,
					// "File Upload Complete.", Toast.LENGTH_SHORT).show();
					// }
					// });
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {

				ex.printStackTrace();

				// runOnUiThread(new Runnable() {
				// public void run() {
				// messageText.setText("MalformedURLException Exception : check script url.");
				// Toast.makeText(MainActivity.this, "MalformedURLException",
				// Toast.LENGTH_SHORT).show();
				// }
				// });

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				e.printStackTrace();

				// runOnUiThread(new Runnable() {
				// public void run() {
				// messageText.setText("Got Exception : see logcat ");
				// Toast.makeText(MainActivity.this,
				// "Got Exception : see logcat ", Toast.LENGTH_SHORT).show();
				// }
				// });
				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}

			return serverResponseCode;

		} // End else block
	}

}
