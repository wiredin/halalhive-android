package com.wiredin.halalhive.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.wiredin.halalhive.NewItemActivity;
import com.wiredin.halalhive.ResultActivity;
import com.wiredin.halalhive.ScanActivity;
import com.wiredin.halalhive.dataclass.DatabaseHandler;
import com.wiredin.halalhive.dataclass.Item;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class ScanService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String barcode;
	String id;

	public ScanService(String barcode, Context context) {
		this.context = context;
		this.barcode = barcode;
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		// do initialization of required objects objects here
		Log.d("Hadian", "Running Scan Service");
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("Halal Hive"); // title
		progressDialog.setMessage("Requesting.."); // message
		progressDialog.setCancelable(false);
		progressDialog.show();

	};

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		Log.d("Hadian", "do in background");
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				"http://wiredin.my/halalhive/services/checkbar/" + barcode);
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// nameValuePairs.add(new BasicNameValuePair("id", this.id));
			// nameValuePairs.add(new BasicNameValuePair("barcode",
			// this.barcode));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (!results.equals("[]")) {

			Log.d("Hadian", "Jadi dgn result: " + results);
			progressDialog.dismiss();

			DatabaseHandler dbHandler = new DatabaseHandler(context, null,
					null, 2);
			dbHandler.deleteScanItem();

			JSONObject[] objects = null;
			try {
				String data = results;
				JSONArray array = new JSONArray(data);
				objects = new JSONObject[array.length()];
				Item[] scanItem = new Item[array.length()];

				for (int i = 0; i < array.length(); i++) {
					objects[i] = array.getJSONObject(i);
					// Log.d("Hadian",
					// "Data: "+array.getJSONObject(i).toString());
					scanItem[i] = new Item(objects[i].getString("id"),
							objects[i].getString("barcode"),
							objects[i].getString("itembar"),
							objects[i].getString("info"),
							objects[i].getString("pic1"),
							objects[i].getString("pic2"),
							objects[i].getString("category"));

					dbHandler.addItem(scanItem[i]);
					Intent intent = new Intent(context, ResultActivity.class);
					context.startActivity(intent);
				}
			} catch (Exception e) {
				Log.d("Hadian", "Error: " + e);
				showError();
			}

		} else {
			Log.d("Hadian", "result null");
			progressDialog.dismiss();
			Intent intent = new Intent(context, NewItemActivity.class);
			intent.putExtra("barcode", barcode);
			context.startActivity(intent);
		}
	}

	public void showError() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		// black
		alertDialogBuilder.setInverseBackgroundForced(true);

		// set title
		alertDialogBuilder.setTitle("Halal Hive");

		// set dialog message
		alertDialogBuilder.setMessage("Not item match").setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
