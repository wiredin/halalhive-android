package com.wiredin.halalhive;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

public class SignUpActivity extends SherlockActivity {
	
	Button btnSignin;
	TextView halal;
	EditText inputUsername, inputPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);
		
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/KGAlwaysAGoodTime.ttf");
		
		halal = (TextView) findViewById(R.id.txt_halal);
		halal.setTypeface(tf);
		
		btnSignin = (Button) findViewById(R.id.btn_signup);
		btnSignin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
		
		inputUsername = (EditText) findViewById(R.id.edit_username);
		inputUsername.setHintTextColor(getResources().getColor(R.color.white));
		
		inputPassword = (EditText) findViewById(R.id.edit_password);
		inputPassword.setHintTextColor(getResources().getColor(R.color.white));
		
	}

	
	
}
