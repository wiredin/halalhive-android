package com.wiredin.halalhive;

import android.os.Bundle;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.wiredin.halalhive.fragment.HomeFragment;
import com.wiredin.halalhive.fragment.InfoFragment;
import com.wiredin.halalhive.fragment.ProfileFragment;
import com.wiredin.halalhive.fragment.ScanFragment;


public class MainActivity extends SherlockFragmentActivity {
	
	//Button btnScan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);

		Tab tab = actionBar
				.newTab()
				.setIcon(R.drawable.ic_bar_home)
				.setTabListener(
						new TabListener<HomeFragment>(this, "Satu",
								HomeFragment.class));
		actionBar.addTab(tab);

		tab = actionBar
				.newTab()
				.setIcon(R.drawable.ic_bar_scan)
				.setTabListener(
						new TabListener<ScanFragment>(this, "Dua",
								ScanFragment.class));
		actionBar.addTab(tab);

		tab = actionBar
				.newTab()
				.setIcon(R.drawable.ic_bar_profile)
				.setTabListener(
						new TabListener<ProfileFragment>(this, "Tiga",
								ProfileFragment.class));
		actionBar.addTab(tab);
		tab = actionBar
				.newTab()
				.setIcon(R.drawable.ic_bar_info)
				.setTabListener(
						new TabListener<InfoFragment>(this, "Empat",
								InfoFragment.class));

		actionBar.addTab(tab);
		
	}
}
