package com.wiredin.halalhive.dataclass;

public class Item {
	private int _id;
	private String item_id;
	private String barcode;
	private String name;
	private String info;
	private String pic1;
	private String pic2;
	private String category;

	public Item() {
		
	}
	
	public Item(int id, String item_id, String barcode, String name, 
			String info, String pic1, String pic2, String category) {
		this._id = id;
		this.item_id = item_id;
		this.name = name;
		this.barcode = barcode;
		this.name = name;
		this.info = info;
		this.pic1 = pic1;
		this.pic2 = pic2;
		this.category = category;
	}
	
	public Item(String item_id, String barcode, String name, 
			String info, String pic1, String pic2, String category) {
		this.item_id = item_id;
		this.name = name;
		this.barcode = barcode;
		this.name = name;
		this.info = info;
		this.pic1 = pic1;
		this.pic2 = pic2;
		this.category = category;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setItem_id(String text){
		this.item_id = text;
	}
	public void setName(String text){
		this.name = text;
	}
	public void setBarcode(String text){
		this.barcode = text;
	}
	public void setInfo(String text) {
		this.info = text;
	}
	public void setPic1(String text){
		this.pic1 = text;
	}
	public void setPic2(String text){
		this.pic2 = text;
	}
	public void setCategory(String text){
		this.category = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getItem_id(){
		return this.item_id;
	}
	public String getName(){
		return this.name;
	}
	public String getBarcode(){
		return this.barcode;
	}
	public String getInfo(){
		return this.info;
	}
	public String getPic1(){
		return this.pic1;
	}
	public String getPic2(){
		return this.pic2;
	}
	public String getCategory(){
		return this.category;
	}

}