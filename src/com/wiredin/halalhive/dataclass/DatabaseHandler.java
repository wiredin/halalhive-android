package com.wiredin.halalhive.dataclass;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "halalhive.db";
	private static final String TABLE_ITEM_SCAN = "item_scan";
	private static final String TABLE_ITEM_VIEW = "item_view";
	
	private static final String ITEM_TABLE_ID = "_id";
	private static final String ITEM_ID = "item_id";
	private static final String ITEM_BARCODE = "item_barcode";
	private static final String ITEM_NAME = "item_name";
	private static final String ITEM_INFO = "item_info";
	private static final String ITEM_PIC1 = "item_pic1";
	private static final String ITEM_PIC2 = "item_pic2";
	private static final String ITEM_CATEGORY = "item_category";
	
	public DatabaseHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		String CREATE_TABLE_ITEM_SCAN = "CREATE TABLE " + TABLE_ITEM_SCAN + "("
				+ ITEM_TABLE_ID + " INTEGER PRIMARY KEY," + ITEM_ID + " TEXT,"
				+ ITEM_BARCODE + " TEXT," + ITEM_NAME + " TEXT," + ITEM_INFO
				+ " TEXT," + ITEM_PIC1 + " TEXT," + ITEM_PIC2 + " TEXT,"
				+ ITEM_CATEGORY + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_ITEM_SCAN);
		String CREATE_TABLE_ITEM_VIEW = "CREATE TABLE " + TABLE_ITEM_VIEW + "("
				+ ITEM_TABLE_ID + " INTEGER PRIMARY KEY," + ITEM_ID + " TEXT,"
				+ ITEM_BARCODE + " TEXT," + ITEM_NAME + " TEXT," + ITEM_INFO
				+ " TEXT," + ITEM_PIC1 + " TEXT," + ITEM_PIC2 + " TEXT,"
				+ ITEM_CATEGORY + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_ITEM_VIEW);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
		Log.v("Hadian", "Database Updated form " + oldVersion + " to "
				+ newVersion);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEM_SCAN);
		onCreate(db);

	}
	
	public void addItem(Item item) {

		ContentValues values = new ContentValues();
		values.put(ITEM_TABLE_ID, 1);
		values.put(ITEM_ID, item.getItem_id());
		values.put(ITEM_BARCODE, item.getBarcode());
		values.put(ITEM_NAME, item.getName());
		values.put(ITEM_INFO, item.getInfo());
		values.put(ITEM_PIC1, item.getPic1());
		values.put(ITEM_PIC2, item.getPic2());
		values.put(ITEM_CATEGORY, item.getCategory());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_ITEM_SCAN, null, values);
		db.close();

		Log.v("Hadian", "scanned item Added");
	}
	
	public void deleteScanItem() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_ITEM_SCAN);
		db.close();
		Log.v("Hadian", "User Deleted");
	}

	public Item getItem() {
		// Select All Query
		int id = 1;

		String query = "Select * FROM " + TABLE_ITEM_SCAN + " WHERE "
				+ ITEM_TABLE_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Item item = new Item();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				item.setId(Integer.parseInt(cursor.getString(0)));
				item.setItem_id(cursor.getString(1));
				item.setBarcode(cursor.getString(2));
				item.setName(cursor.getString(3));
				item.setInfo(cursor.getString(4));
				item.setPic1(cursor.getString(5));
				item.setPic2(cursor.getString(6));
				item.setCategory(cursor.getString(7));

			} while (cursor.moveToNext());

			Log.v("Hadian", "User -" + item.getName());
		} else {
			item = null;
		}
		db.close();

		// return contact list
		return item;
	}

}
