package com.wiredin.halalhive;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.squareup.picasso.Picasso;
import com.wiredin.halalhive.dataclass.DatabaseHandler;
import com.wiredin.halalhive.dataclass.Item;

public class ResultActivity extends SherlockActivity {
	
	String value;
	TextView txtName, txtBarcode, txtInfo;
	ImageView pic1, pic2;
	Item scanItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result_activity);
		
		DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 2);
		scanItem = dbHandler.getItem();
		
		Log.d("Hadian", "value recieve: " + scanItem);
		txtName = (TextView) findViewById(R.id.txt_name);
		txtName.setText(scanItem.getName());
		
		txtBarcode = (TextView) findViewById(R.id.txt_barcode);
		txtBarcode.setText(scanItem.getBarcode());
		
		txtInfo = (TextView) findViewById(R.id.txt_info);
		txtInfo.setText(scanItem.getInfo());
		
		pic1 = (ImageView) findViewById(R.id.imageView1);
		pic2 = (ImageView) findViewById(R.id.imageView2);
		
		Picasso.with(this)
		.load("http://www.wiredin.my/halalhive/" + scanItem.getPic1())
		.resize(360, 360).centerCrop().into(pic1);
		
		Picasso.with(this)
		.load("http://www.wiredin.my/halalhive/" + scanItem.getPic2())
		.resize(360, 360).centerCrop().into(pic2);
		
		
	}

	
	
}
