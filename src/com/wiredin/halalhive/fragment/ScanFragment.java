package com.wiredin.halalhive.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.wiredin.halalhive.MainActivity;
import com.wiredin.halalhive.R;
import com.wiredin.halalhive.ScanActivity;

public class ScanFragment extends SherlockFragment {
	
	Button btnScan;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.open_scanner_fragment, container, false);
		
		btnScan = (Button) rootView.findViewById(R.id.btn_scan);
		btnScan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), ScanActivity.class);
				startActivity(intent);
			}
		});
		
		return rootView;
	}
	
	

}
